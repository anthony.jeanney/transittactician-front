# TransitTactician React Frontend

# TransitTactician
# Projet Innovant - M2 WEDSCI, M1 MAI 

# Membres

#### M2 WeDSci (Developpeurs)

- Jeanney Anthony - `anthony.jeanney@gmail.com`
- Leprêtre Hugo - `lephugo@outlook.fr`
- Dumont Quentin - `quentin1.dumont@gmail.com`
- Leroy Laury - `leroy.laury62@gmail.com`


#### M1 MAI (Marketing)

- Badassou Kpego Kokou - `badakokou82@gmail.com`
- Montcho Mauriac - `montcho113@yahoo.com`
- Talilt Wafae - `wafaetalilt@gmail.com`


# Résumé

Transit tactitian est une application de prédiction de retards ou d'annulations
et de réservations des trains SNCF.

Son but est la prédiction à l'aide de data (intempéries, travaux) de ces derniers
et de proposer des trajets alternatifs par d'autres services tels que le covoiturage, le métro
et cetera.

Des reservations de billets peuvent être effectuées dans l'application, aucun frais supplémentaires
ne sont appliqués.

# Démarrer le Frontend

### `npm start`

Ouvrez [http://localhost:3000](http://localhost:3000) dans votre navigateur pour voir l'application.