import './App.css';
import React, { useState } from 'react';
import icon from './assets/icon.png';
import axios from 'axios';

function App() {

  const [departure, setDeparture] = useState('');
  const [arrival, setArrival] = useState('');
  const [date, setDate] = useState('');
  const [time, setTime] = useState('');

  const [results, setResults] = useState([]);

  const handleSubmit = (e) => {
    e.preventDefault();
    axios.get(`http://localhost:5024/api/Trip/tripsByNameAndTimes?departure=${departure}&arrival=${arrival}&wantedArrivalTime=${date}T${time}:00.000Z`)
      .then((response) => {
        setResults(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  }

  const inverseDepartureAndArrival = () => {
    const temp = departure;
    setDeparture(arrival);
    setArrival(temp);
  }

  return (
    <div className="App">
      <header className="App-header">
        <img src={icon} className="App-logo" alt="logo" />
        <p>TransitTactician</p>
      </header>
      <div className="App-body">
        <div className="selection">
          <div className="form-group">
            <label htmlFor="departure">Departure:</label>
            <input type="text" className="form-control" id="departure" value={departure} onChange={(e) => setDeparture(e.target.value)} />
          </div>
          <button className="btn btn-secondary" onClick={inverseDepartureAndArrival}><i className="bi bi-arrow-down-up"></i></button>
          <div className="form-group">
            <label htmlFor="arrival">Arrival:</label>
            <input type="text" className="form-control" id="arrival" value={arrival} onChange={(e) => setArrival(e.target.value)} />
          </div>
          <div className="form-group">
            <label htmlFor="date">Arrival Date:</label>
            <input type="date" className="form-control" id="date" value={date} onChange={(e) => setDate(e.target.value)} />
            <input type="time" className="form-control" id="time" value={time} onChange={(e) => setTime(e.target.value)} />
          </div>
          <button type="submit" className="btn btn-primary" onClick={handleSubmit}>Submit</button>
        </div>
        <div className="results table-responsive">
          {results.length > 0 && (
          <table className="table table-striped">
            <thead>
              <tr>
                <th>Name</th>
                <th>Departure</th>
                <th>Arrival</th>
                <th>Departure Time</th>
                <th>Arrival Time</th>
                <th>Predicted Lateness</th>
              </tr>
            </thead>
            <tbody>
              {results.map((result, index) => {
                return (
                  <tr key={index}>
                  <td>{result.route.shortName}</td>
                  <td>{result.departureStop.name}</td>
                  <td>{result.arrivalStop.name}</td>
                  <td>{result.departureStopTime.departureTime.hours.toString().padStart(2, '0')} : {result.departureStopTime.departureTime.minutes.toString().padStart(2, '0')} : {result.departureStopTime.departureTime.seconds.toString().padStart(2, '0')}</td>
                  <td>{result.arrivalStopTime.arrivalTime.hours.toString().padStart(2, '0')} : {result.arrivalStopTime.arrivalTime.minutes.toString().padStart(2, '0')} : {result.arrivalStopTime.arrivalTime.seconds.toString().padStart(2, '0')}</td>
                  <td>{result.predictedLateness} min</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
              )}
        </div>
      </div>
    </div>
  );
}

export default App;
